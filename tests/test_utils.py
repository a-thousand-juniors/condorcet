import pytest

from condorcet import utils


@pytest.mark.parametrize(
    "candidate_0, candidate_0_score, candidate_1, candidate_1_score",
    [
        ("killians", 37, "molson", 18),
        ("killians", 26, "samuel_adams", 29),
        ("killians", 16, "guinness", 39),
        ("killians", 22, "meister_brau", 33),
        ("molson", 18, "samuel_adams", 37),
        ("molson", 18, "guinness", 37),
        ("molson", 18, "meister_brau", 37),
        ("samuel_adams", 43, "guinness", 12),
        ("samuel_adams", 27, "meister_brau", 28),
        ("guinness", 19, "meister_brau", 36),
    ],
)
def test_evaluate_votes_for_candidate_pair(
    request, candidate_0, candidate_0_score, candidate_1, candidate_1_score, votes
):
    candidate_0 = request.getfixturevalue(candidate_0)
    candidate_1 = request.getfixturevalue(candidate_1)

    assert utils.evaluate_votes_for_candidate_pair(candidate_0, candidate_1, votes) == {
        candidate_0: candidate_0_score,
        candidate_1: candidate_1_score,
    }


def test_tabulate_pairwise_results(pairwise_results, result_table):
    assert utils.tabulate_pairwise_results(pairwise_results) == result_table


def test_get_winner_from_result_table(result_table, cyclic_result_table, meister_brau):
    assert utils.get_winner_from_result_table(result_table) == meister_brau
    assert utils.get_winner_from_result_table(cyclic_result_table) is None


@pytest.mark.parametrize(
    "candidate", ["killians", "molson", "samuel_adams", "guinness", "meister_brau"]
)
def test_drop_candidate_from_result_table(request, candidate, result_table):
    candidate = request.getfixturevalue(candidate)
    pruned_table = utils.drop_candidate_from_result_table(candidate, result_table)

    assert candidate not in pruned_table
    for wins_and_losses in pruned_table.values():
        assert candidate not in wins_and_losses["wins"]
        assert candidate not in wins_and_losses["losses"]


@pytest.mark.parametrize("num_winners", range(1, 6))
def test_get_n_winners_from_result_table(num_winners, result_table, win_order):
    winners, pruned_table = utils.get_n_winners_from_result_table(
        num_winners, result_table
    )

    assert winners == win_order[:num_winners]
    for wins_and_losses in pruned_table.values():
        for winner in winners:
            assert winner not in wins_and_losses["wins"]
            assert winner not in wins_and_losses["losses"]


def test_get_n_winners_from_cyclic_result_table(cyclic_result_table):
    winners, pruned_table = utils.get_n_winners_from_result_table(
        3, cyclic_result_table
    )

    assert winners == []
    assert pruned_table == cyclic_result_table
