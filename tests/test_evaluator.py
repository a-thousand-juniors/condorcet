import pytest

from condorcet.evaluator import CondorcetEvaluator


def test_instantiation(candidates, votes, result_table):
    evaluator = CondorcetEvaluator(candidates=candidates, votes=votes)

    assert evaluator.result_table == result_table


@pytest.mark.parametrize("num_winners", range(3, 5))
@pytest.mark.parametrize("table", ["result_table", "cyclic_result_table"])
def test_get_n_winners(request, mocker, num_winners, table, candidates, votes):
    table = request.getfixturevalue(table)
    mocker.patch.object(
        CondorcetEvaluator, "_compute_result_table", return_value=table, autospec=True
    )
    mock_get_winners = mocker.patch(
        "condorcet.utils.get_n_winners_from_result_table", autospec=True
    )

    evaluator = CondorcetEvaluator(candidates=candidates, votes=votes)
    winners = evaluator.get_n_winners(num_winners)

    mock_get_winners.assert_called_once_with(num_winners, table)
    assert winners == mock_get_winners.return_value
